from django.conf.urls.defaults import patterns, include, url

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
from mailing_list_managment import mlmanage
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'mailing_list_managment.views.home', name='home'),
    # url(r'^mailing_list_managment/', include('mailing_list_managment.foo.urls')),

    url(r'^$', 'mlmanage.views.index'),
    #url(r'^mlm/(?P<poll_id>\d+)/$', 'mlmanage.views.detail'),
    #url(r'^mlm/(?P<poll_id>\d+)/results/$', 'mlmanage.views.results'),
    #url(r'^mlm/(?P<poll_id>\d+)/vote/$', 'mlmanage.views.vote'),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
)
