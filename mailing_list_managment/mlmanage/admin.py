from mlmanage.models import Task, Job
from django.contrib import admin

admin.site.register(Task)
admin.site.register(Job)
