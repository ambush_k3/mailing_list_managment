# -*- coding: utf-8 -*-

from django.db import models


TIMES_CHOICES = (
        ('09:00', '09:00'), ('09:30', '09:30'),
        ('10:00', '10:00'), ('10:30', '10:30'),
        ('11:30', '11:30'), ('12:00', '12:00'),
        ('12:30', '12:30'), ('13:00', '13:00'),
        ('13:30', '13:30'), ('14:00', '14:00'),
)

STATUS_CHOICES = (('none', ''),
                  ('ready', 'ready'),
                  ('changed', 'Changed'))


class Task(models.Model):
    name = models.CharField('Monitoring cycle', max_length=200)
    command = models.CharField(max_length=200)

    def __unicode__(self):
        return self.name


class Job(models.Model):
    task = models.ForeignKey(Task)
    time = models.CharField('Time delivery', max_length=15,
                            choices=TIMES_CHOICES, default='Not selected')
    #FIXME: not implemented yet
    #status = models.CharField(max_length=20,
    #                          choices=STATUS_CHOICES)
    sign = models.CharField(max_length=10)

    def __unicode__(self):
        return self.task.name
        #return Task.objects.get(job__pk__contains=self.task).name
