# -*- coding: utf-8 -*-
from django import forms
from mlmanage.models import Job, Task
import mlmanage


class TableForm(forms.Form):
    # In new records id = ''
    job_id = forms.IntegerField(widget=forms.HiddenInput(), required=False)
    task_id = forms.ChoiceField(label='',
            choices=[(task.id, task.name) for task in Task.objects.all()])
    job_time = forms.ChoiceField(label='',
            choices=mlmanage.models.TIMES_CHOICES)
