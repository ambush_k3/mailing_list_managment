# -*- coding: utf-8 -*-
from mlmanage.models import Job, Task
from mlmanage.forms import TableForm
import mlmanage
from django.shortcuts import render_to_response, get_object_or_404
from django.utils.datastructures import MultiValueDictKeyError
from mlmanage import cron


def job_save(task_id, job_time, job_id):
    ''' create and change jobs '''
    from random import randint

    if job_id == '':
        #Create new record
        record = Job()
        action = 'new'
        #TODO: verify that the record don't exist
        record.sign = randint(00000, 99999)
    else:
        record = get_object_or_404(Job, pk=job_id)
        action = 'edit'
    record.task_id = task_id
    record.time = job_time
    record.save()

    command = record.task.command
    cron.cron(record.sign, action, job_time, command)


def index(request):
    '''Shows 'add job' button to create new job and list of existing jobs'''

    if request.method == 'GET':
        form = TableForm(request.GET)
        try:
            request.GET['new_job']
        except MultiValueDictKeyError:
            try:
                request.GET['delete']
            except MultiValueDictKeyError:
                # possibly correct form
                if form.is_valid():
                    job_save(task_id=request.GET['task_id'],
                                 job_time=request.GET['job_time'],
                                 job_id=request.GET['job_id'])
            else:
                # delete job
                record = get_object_or_404(Job, pk=request.GET['job_id'])
                record.delete()
                action = 'delete'
                cron.cron(record.sign, action,
                          record.time, record.task.command)
        else:
            # this is new job
            job_list = [TableForm()]
            return render_to_response('mlmanage/index.html',
                              {'job_list': [job_list],
                               'new': True})

    job_list = []
    # reverse to show new records on top
    for job in Job.objects.all().order_by('id').reverse():
        job_list.append([
                     TableForm(initial={'job_id': job.id,
                                        'task_id': job.task.id,
                                        'job_time': job.time,
                                        })])

    return render_to_response('mlmanage/index.html',
                              {'job_list': job_list,
                               })


